/**
 * Created by AndreyMaznyak on 22.09.2016.
 */
'use strict';

module.exports = {
    products:{
        connection:'couch',
        //tableName: 'operator',
        schema: true,
        //migrate: 'alter',

        attributes: {
            id: {
                primaryKey: true,
                type: 'string',
                unique:true
            },
            parentid:{
                type: 'string',
                required:true,
                index: true
            },
            subitems_count:{
              type:'integer',
              required: true,
              index:true
            },
            // barcode:{
            //   type: 'string'
            // },
            isfolder:{
                type:'boolean',
                defaultsTo:false
            },
            name:{
                type:'string',
                required:true
            },
            code:{
                type:'string',
                required:true
            },
            barcode:{
                type:'string'
            },
            //Оптовая цена
            wholesale_price:{
                type: 'float'
                //decimal2: true
            },
            //Розничная цена
            retail_price:{
                type: 'float'
                //decimal2: true
            },
            //Остаток
            count:{
                type:'integer'
            },
            reserve:{
                type:'integer'
            },
            //минимальная продажа
            min_sale:{
                type:'integer',
                required:true
            }

        },
        types: {
            decimal2: function(number){
                return ((number *100)%1 === 0);
            }
        }
    }
};